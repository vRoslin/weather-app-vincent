export 

// Skapa datum som visas på hemsidan
const createDate = (fullDate) => {
    let months = [
      "January", 
      "February", 
      "March", 
      "April", 
      "May", "June", 
      "July", 
      "August", 
      "September", 
      "October", 
      "November", 
      "December"
    ];
    let days = [
      "Sunday", 
      "Monday", 
      "Tuesday", 
      "Wednesday", 
      "Thursday", 
      "Friday", 
      "Saturday"
    ];

    let day = days[fullDate.getDay()]; // Veckodag
    let date = fullDate.getDate(); // Dagens datum
    let month = months[fullDate.getMonth()]; // Månad
    let year = fullDate.getFullYear(); // År

    return `${day} ${date} ${month} ${year}`
  }