
import React, { useState } from 'react'
import { createDate } from './createDate'
import { api } from './api'

function App() {
  const [query, setQuery] = useState('');
  const [weather, setWeather] = useState({});

  const search = evt => {
    if (evt.key === "Enter") { // Klicka enter för att söka
      fetch(`${api.base}weather?q=${query}&units=metric&APPID=${api.key}`) //API som kallar på vädret
        .then(res => res.json())
        .then(result => {
          setWeather(result);
          setQuery('');
          console.log(result);
        });
    }
  }

  return (
    <div className="App">
      <main>
        <div className="Search">
          <input 
            type="text"
            className="SearchBar"
            placeholder="Enter city name..."
            onChange={e => setQuery(e.target.value)} //Ändrar state till staden
            value={query}
            onKeyPress={search}
          />
        </div>
        {(typeof weather.main != "undefined") ? (
        <div className="LocationWrapper">
          <div className="Location">
            {/* Visar vilken stad samt namn */}
            <div className="LocationInfo">{weather.name}, {weather.sys.country}</div>
            
            <div className="Date">{createDate(new Date())}</div>
          </div>
          <div className="Weather">
            <div className="Temperature">
              {/* Rundar av temperaturen så det inte ser knasigt ut */}
              {Math.round(weather.main.temp)}°C
            </div>
            <div className="WeatherInfo">
              {/* Visar vilken typ av väder ex. molnigt eller soligt */}
              {weather.weather[0].main}
            </div>
            <div className="WeatherIcon">
              {/* Bild som ändras efter vilken typ av väder det är */}
              <img src={`http://openweathermap.org/img/wn/${weather.weather[0].icon}@2x.png`} alt="icon"/>
            </div>
          </div>
        </div>
        ) : ('')}
      </main>
    </div>
  );
}

export default App;
